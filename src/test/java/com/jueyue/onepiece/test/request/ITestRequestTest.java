package com.jueyue.onepiece.test.request;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.jueyue.onepiece.test.entity.BaiduWeatherEntity;
import com.jueyue.onepiece.test.spring.SpringTxTestCase;
import com.onepiece.requestproxy.util.JSONUtil;

public class ITestRequestTest extends SpringTxTestCase {

    @Autowired
    private ITestRequest testRequest;

    @Test
    public void testGet() {
        String re = testRequest.testGet("北京", "json", "5slgyqGDENN7Sy7pw29IUvrZ");
        System.out.println(re);

        BaiduWeatherEntity entity = JSONUtil.parseJson(re, BaiduWeatherEntity.class);

        System.out.println(entity.getStatus());

        entity = testRequest.testGetEntity("北京", "json", "5slgyqGDENN7Sy7pw29IUvrZ");
        System.out.println(JSONUtil.toJson(entity));

    }

}
