package com.onepiece.requestproxy.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Json工具类
 * 
 * @author JueYue
 * @date 2014年2月24日 下午7:25:57
 */
@SuppressWarnings("unchecked")
public final class JSONUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JSONUtil.class);

    private static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private static JavaType getCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
        return mapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }

    public static final <T> T parseJson(String json, Class<?> clss) {
        try {
            return (T) mapper.readValue(json, clss);
        } catch (JsonProcessingException e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error("解析JSON串失败", e.fillInStackTrace());
        } catch (Exception e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error("解析JSON串失败", e.fillInStackTrace());
        }
        return null;
    }

    public static final <T> List<T> parseJsonList(String json, Class<?> clss) {
        JavaType javaType = getCollectionType(ArrayList.class, clss);
        try {
            return mapper.readValue(json, javaType);
        } catch (JsonParseException e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error("解析JSON串失败", e.fillInStackTrace());
        } catch (JsonMappingException e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error("解析JSON串失败", e.fillInStackTrace());
        } catch (Exception e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error("解析JSON串失败", e.fillInStackTrace());
        }
        return null;
    }

    public static final String toJson(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            LOGGER.error("JSON序列化失败:{}", obj);
            LOGGER.error("JSON序列化失败", e.fillInStackTrace());
        } catch (Exception e) {
            LOGGER.error("JSON序列化失败:{}", obj);
            LOGGER.error("JSON序列化失败", e.fillInStackTrace());
        }
        return null;
    }

}
